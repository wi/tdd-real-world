# Testing Workshop in an Spring codebase
This is a two part exercise.

1. Writing a test "backwards" to add coverage of existing code (not TDD)
1. Apply TDD to implement a new feature

The project is the standard ["Pet clinic" Spring demo application](https://github.com/spring-projects/spring-petclinic).

## Setup and running tests (with coverage)

Clone the repo, then import the project in IntelliJ Idea. Run all tests with coverage. To get a more detailed coverage report (allowing you to see which tests cover what code), you can enable tracing:

<video controls width="100%">
    <source src="./run-tests-with-coverage-idea.mov">
    Sorry, your browser doesn't support embedded videos.
</video>


## A tour of the code

Once the test run completes, navigate to the `org.springframework.samples.petclinic.owner.Owner#getPet` method that has two parameters. Notice the `for`-loop isn't covered by any tests. Notice also that the comment is identical for both the single and two parameter methods. So, some "business logic" is missing in this description.

![coverage for getPet](./coverage-for-getpet.png)

First, figure out what `Pet.isNew` means. That is, when does it return `true`? Then figure out in what controller the method is being called.

Controllers in this (and most other) projects integrate with two stateful components:
1. A web server (a.k.a. servlet container)
1. and a database

The existing tests simulate real web requests, but mock the database. The tests use mocks/stubs for the `Repository` beans using `@MockBean`. This speeds up execution and limits the scope to our own code.

```java
class PetControllerTests {
    // ...
	@MockBean
	private PetRepository pets;
	@MockBean
	private OwnerRepository owners;

	@BeforeEach
	void setup() {
		PetType cat = new PetType();
		cat.setId(3);
		cat.setName("hamster");
		given(this.pets.findPetTypes()).willReturn(Lists.newArrayList(cat));
		given(this.owners.findById(TEST_OWNER_ID)).willReturn(new Owner());
		given(this.pets.findById(TEST_PET_ID)).willReturn(new Pet());
	}
    // ...
}
```

# Exercise 1

Write a test that covers the currently missing lines in the `PetController#processCreationForm` and (by extension) the `Owner#getPet` methods. 

Look at other controller tests to see how to test them.

Try to apply the "working backwards" (_THEN ➡ WHEN ➡ GIVEN_) strategy:

1. Write the assertion/expectation first. Use variable/method names that produce the most readable code.
1. Declare any missing variables/functions.
1. Continue by writing the interaction (calling the controller).
1. Finally, write the initialization code.

Because the implementation already exists, we're not doing TDD. However, we still get the benefit of working backwards, because we write the _ideal_ statements (with thoughtful names) before we make the code work. The test should read like a functional requirement.

# Exercise 2 A

Apply TDD to implement a new feature (happy path):

1. Given the user has a list of new pet data
1. When the user saves the new pets (makes a `POST` request to `/owners/{ownerId}/pets/newBatch`)
1. Then the pets are saved
1. And the user is redirected to the owner page (i.e. `/owners/{ownerId}`)

# Exercise 2 B

Apply TDD again to implement the following additional feature (same end-point, but unhappy path):

1. Given the user has a list of new pet data that has invalid data (fails validation rules)
1. When the user saves the new pets (makes a `POST` request to `/owners/{ownerId}/pets/newBatch`)
1. Then the pets are not saved
1. And the user is returned to pet edit page (i.e. the `pets/createOrUpdatePetForm` view)
1. And the response contains the error information (the model attribute has errors)
