import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroesComponent } from './heroes.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HeroService } from '../hero.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

fdescribe('HeroesComponent', () => {
  let component: HeroesComponent;
  let fixture: ComponentFixture<HeroesComponent>;
  let heroServiceStub: any;
  let nameInput: HTMLInputElement
  let addButton: HTMLButtonElement

  beforeEach(async(() => {
    heroServiceStub = jasmine.createSpyObj('HeroService', ['getHeroes', 'addHero']);
    heroServiceStub.getHeroes.and.returnValue(of([{ name: "Some name" }]))
    heroServiceStub.addHero.and.callFake((hero) => of(hero))
    TestBed.configureTestingModule({
      declarations: [HeroesComponent],
      providers: [
        {
          provide: HeroService,
          useValue: heroServiceStub
        }
      ],
      imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    nameInput = fixture.nativeElement.querySelector('.name-input')
    addButton = fixture.debugElement.query(By.css('.add-button')).nativeElement
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call getHeroes on initialization', (done) => {
    expect(heroServiceStub.getHeroes).toHaveBeenCalled()
    whenHeroesLoaded((heroes) => {
      expect(heroes).toBeTruthy()
      fixture.detectChanges()
      const numberOfBadges = heroListItems().length
      expect(numberOfBadges).toBe(1)
      done()
    })
  })

  it('should show an added hero at the bottom', (done) => {
    whenHeroesLoaded(() => {
      const heroName = "Test name"
      // when I fill in a name in the input
      // and I click the add button
      nameInput.value = heroName
      addButton.click()
      // then the service method should be called
      expect(heroServiceStub.addHero).toHaveBeenCalled()
      // and the last hero-name contains the added name
      fixture.detectChanges()
      expect(lastOf(heroNames()).textContent).toBe(heroName)
      done()
    })
  })

  function lastOf(list) {
    return list[list.length - 1]
  }

  function heroNames() {
    return fixture.nativeElement.querySelectorAll('.hero-name')
  }
  function whenHeroesLoaded(callback) {
    component.getHeroesObservable.subscribe(callback)
  }

  function heroListItems() {
    return fixture.nativeElement.querySelectorAll('.heroes li')
  }
});
