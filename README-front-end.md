# Testing Workshop in an Angular codebase
This is a two part exercise.

1. Writing a test "backwards" to add coverage of existing code (not TDD)
1. Apply TDD to implement a new feature

The project is the standard ["Tour of Heroes"](https://stackblitz.com/angular/xedqnmymokn?file=src%2Fapp%2Fhero.service.ts) application from the Angular tutorial. If you want your own copy, sources can be downloaded [here](https://angular.io/generated/zips/toh-pt6/toh-pt6.zip).

## Setup and running tests (with coverage)

Clone the repo, then use these commands to run the tests.

```shell
cd angular-front-end
npm i
npx ng test --code-coverage
```

## A tour of the code

Open the coverage report in a browser:

```shell
open ./coverage/angular.io-example/index.html
```

Only the tests in `src/app/heroes/heroes.component.spec.ts` are being executed. Open that file in your editor. Notice this line:

```typescript
fdescribe('HeroesComponent', () => {
```

Thanks to `fdescribe` only the tests in this block are executed. 

In the coverage report (which you opened in a browser window), navigate to the `heroes.component.ts` file. There is one branch in the `add` method, and the whole `delete` method not being covered by tests.

The `HeroesComponent` integrates with two APIs:

1. The browser DOM API
1. and the `HeroService` (which abstracts the web API)

In our tests we choose to **stub** the `HeroService`:

```typescript
  heroServiceStub = jasmine.createSpyObj('HeroService', ['getHeroes', 'addHero']);
```

The stub allows us to limit the scope of the test and (sometimes) speed up the execution.

# Exercise 1 A

Write a test to cover the missing branch (the red `return` statement in the below screenshot).

![coverage](./screenshot-coverage.png)

Apply the "working backwards" (_THEN ➡ WHEN ➡ GIVEN_) strategy:

1. Write the assertion/expectation first. Use variable names that produce the most readable code.
1. Declare any missing variables/functions.
1. Continue by writing the interaction (calling the component methods).
1. Finally, write the initialization code, if any.

Because the implementation (component) already exists, we're not doing TDD. However, we still get the benefit of working backwards, because we write the _ideal_ statements (with thoughtful names) before we make the code work.

# Exercise 1 B

Write a test that covers the `delete` method. Because our service stub already loads a single list-item, we could simply delete the item that's already there from the start.

Again, apply the "working backwards" strategy and choose the names carefully.

# Exercise 2

Apply TDD to implement a new feature:

1. Given a loaded hero list
1. When the user reloads the list (i.e. clicks on a reload button)
1. Then the list is loaded again
1. And newly loaded heroes are displayed

Work backwards. However, in between you will also have to write the implementation. After the test is finished, refactor as you feel necessary, including the test code.