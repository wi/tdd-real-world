# TDD In realistic (stateful) code bases

Choose your destiny:

1. [Angular-based front end application](./README-front-end.md)
1. [Spring-based web-application](./README-back-end.md)

# Useful resources

* [The TDD Workshop's Presentation](https://docs.google.com/presentation/d/1p-d5Bd7cuSYsN4zsGLHeABVyYcTOqcStPh_jCA8CAH4/edit?usp=sharing)
* [Outside-in TDD](https://8thlight.com/blog/georgina-mcfadyen/2016/06/27/inside-out-tdd-vs-outside-in.html)
* [Angular Testing Dev Documentation](https://angular.io/guide/testing)
* [Spring Boot Testing Documentation](https://docs.spring.io/spring-boot/docs/1.5.2.RELEASE/reference/html/boot-features-testing.html)
* [TDD good habits manifesto](https://github.com/neomatrix369/refactoring-developer-habits/blob/master/02-outcome-of-collation/tdd-manifesto/tdd-good-habits-manifesto.md)
